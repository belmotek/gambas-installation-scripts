#!/bin/bash
# Gambas installation scripts
# Install gambas development version (master)
# Manjaro
# Belmotek: https://gitlab.com/belmotek/gambas-installation-scripts
#===============================================================================

sudo pacman -Syu --needed --noconfirm alure autoconf automake bzip2 zstd coreutils curl dbus gcc gdk-pixbuf2 git glew glib2 gmime gsl gst-plugins-base gstreamer gtk2 gtk3 gtkglext imlib2 intltool libffi libgl libgnome-keyring libmariadbclient librsvg libsm libxcursor libxml2 libxslt libxtst mariadb make mesa ncurses pcre

sudo pacman -Syu --needed --noconfirm pkg-config poppler poppler-glib postgresql postgresql-libs qt5-svg qt5-webkit qt5-x11extras sdl2 sdl2_gfx sdl2_image sdl2_mixer sdl2_net sdl2_ttf sdl_mixer sdl_ttf smpeg sqlite unixodbc v4l-utils xdg-utils zlib gettext qt5-webengine webkit2gtk

mkdir /tmp/gambasins

git clone --depth=1 https://gitlab.com/gambas/gambas.git /tmp/gambasins

cd /tmp/gambasins

./reconf-all
GAMBAS_CONFIG_FAILURE=1 ./configure -C --disable-sqlite2 --disable-qt4
make -j$(nproc) &> /tmp/gambas-make.log
sudo make install &> /tmp/gambas-install.log



